#
#  Be sure to run `pod spec lint HeyHubSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '12.1'
s.name = "DemoSDK"
s.summary = "DemoSDK lets a user select an ice cream flavor."
#s.requires_arc = true

# 2
s.version = "0.2.0"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "Shridhar Sawant" => "shridharsawant111@gmail.com" }

# 5 - Replace this URL with your own GitHub page's URL (from the address bar)
s.homepage = "https://bitbucket.org/shirya47/heyhubsdk/"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://shirya47@bitbucket.org/shirya47/heyhubsdk.git", 
		       :tag => "#{s.version}" }

# 7
#s.framework = "UIKit"
s.ios.dependency 'Alamofire', '~> 5.4'

# 8
#s.source_files = "DemoSDK.framework/**/*{h,m,swift}"
s.exclude_files = "DemoSDK.framework/**/*{.plist}"
s.public_header_files = "HeyHubSDK.framework/Headers/*{h,swift}"
s.vendored_frameworks = "Add/**/*{.xcframework}"

# 9
#s.resources = "HeyHubSDK.framework/**/*.{png,jpeg,jpg,json,plist,nib,xcassets}"

# 10
s.swift_version = "4.2"

end

